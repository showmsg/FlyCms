package com.flycms.module.security.dao;

import com.flycms.module.security.model.Role;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Open source house, All rights reserved
 * 开发公司：28844.com<br/>
 * 版权：开源中国<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 2018/7/9
 */
@Repository
public interface RoleDao {

    // ///////////////////////////////
    // /////      增加        ////////
    // ///////////////////////////////
    //添加权限小组名
    public int addRole(Role role);

    // ///////////////////////////////
    // /////       修改       ////////
    // ///////////////////////////////
    //按id修改组名
    public int updateRole(@Param("name") String name, @Param("id") int id);

    //权限组id和权限id关联
    public int addRolePermission(@Param("roleId") int roleId, @Param("permissionId") int permissionId);
    // ///////////////////////////////
    // /////        删除      ////////
    // ///////////////////////////////
    //按id删除角色信息
    public int deleteRole(@Param("id") int id);

    //按id删除角色和权限关联信息
    public int deleteRolePermission(@Param("roleId") Integer roleId, @Param("permissionId") Integer permissionId);

    // ///////////////////////////////
    // /////       查询       ////////
    // ///////////////////////////////
    //按ID查询权限组信息
    public Role findRoleById(@Param("id") int id);

    //查询用户组名是否存在
    public int checkRole(@Param("name") String name);

    //查询所有权限组
    public int getRoleCount();

    //权限组列表
    public List<Role> getRoleList(@Param("offset") int offset, @Param("rows") int rows);

    //所有权限小组列表
    public List<Role> getAllRoleList();

    //按管理员id查询所在会员组id
    public Integer findAdminAndRoleById(@Param("adminId") int adminId);

    //按管理员id查询所在会员组信息
    public Role findAdminByRole(@Param("adminId") int adminId);
}
