package com.flycms.web.admin;

import com.flycms.core.base.BaseController;
import com.flycms.core.entity.DataVo;
import com.flycms.core.entity.PageVo;
import com.flycms.module.user.model.UserGroup;
import com.flycms.module.user.model.UserPermission;
import com.flycms.module.user.service.UserGroupService;
import com.flycms.module.user.service.UserPermissionService;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.LinkedHashMap;
import java.util.List;


@Controller
@RequestMapping("/admin/grouprole")
public class GroupAdminController extends BaseController {
    @Autowired
    protected UserPermissionService userPermissionService;

    @Autowired
    protected UserGroupService userGroupService;

    //同步所有权限
    @GetMapping("/sync")
    @ResponseBody
    public DataVo getSyncAllPermission(){
        if(userPermissionService.getSyncAllPermission()){
            return DataVo.success("同步权限成功");
        }
        return DataVo.failure("同步权限失败");
    }

    @GetMapping(value = "/assignPermissions/{id}")
    public String assignPermissions(@PathVariable int id, ModelMap modelMap){
        UserGroup group = userGroupService.findUserGroupByid(id);
        List<UserPermission> permissionList = userPermissionService.getAllPermissions();
        LinkedHashMap<String, List<UserPermission>> permissionMap = userPermissionService.groupByController(permissionList);
        modelMap.put("group", group);
        modelMap.put("permissionMap", permissionMap);
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("user/assign_permissions");
    }

    @ResponseBody
    @PostMapping(value = "/markpermissions")
    public DataVo addRole(@RequestParam(value = "groupId", defaultValue = "0") Integer groupId,@RequestParam(value = "permissionId", defaultValue = "0") Integer permissionId) {
        DataVo data = DataVo.failure("操作失败");
        if(groupId==0 || permissionId==0){
            return data.failure("参数错误！");
        }
        if(userPermissionService.markAssignedPermissions(groupId,permissionId)){
            if(userGroupService.deleteUserGroupPermission(groupId,permissionId)){
                return data.success("删除成功！");
            }else{
                return data.failure("删除失败！");
            }
        }else{
            if(userGroupService.addUserGroupPermission(groupId ,permissionId)){
                return DataVo.success("添加成功！");
            }else{
                return DataVo.failure("添加失败！");
            }
        }
    }

    //删除权限节点
    @PostMapping("/role_del")
    @ResponseBody
    public DataVo deletePermission(@RequestParam(value = "id") int id){
        DataVo data = DataVo.failure("操作失败");
        if(userPermissionService.deletePermission(id)){
            data = DataVo.success("该权限已删除");
        }else{
            data = DataVo.failure("删除失败或者不存在！");
        }
        return data;
    }

    @GetMapping(value = "/update/{id}")
    public String updatePermissions(@PathVariable int id, ModelMap modelMap){
        UserPermission permission = userPermissionService.findPermissionById(id);
        modelMap.put("permission", permission);
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("user/group_role_update");
    }

    //处理用户组信息
    @PostMapping("/update_save")
    @ResponseBody
    public DataVo updateGrouupPermissionsSave(@Valid UserPermission permission, BindingResult result){
        DataVo data = DataVo.failure("操作失败");
        try {
            if (result.hasErrors()) {
                List<ObjectError> list = result.getAllErrors();
                for (ObjectError error : list) {
                    return DataVo.failure(error.getDefaultMessage());
                }
                return null;
            }
            data = userPermissionService.updatePermissions(permission);
        } catch (Exception e) {
            data = DataVo.failure(e.getMessage());
        }
        return data;
    }

    //用户组排序处理
    @PostMapping("/groupsort_update")
    @ResponseBody
    public DataVo updateGroupSort(@RequestParam(value = "id", required = false) String id,@RequestParam(value = "sort", required = false) String sort){
        DataVo data = DataVo.failure("操作失败");
        try {
            if (!NumberUtils.isNumber(id)) {
                return data=DataVo.failure("话题参数错误");
            }
            if (!NumberUtils.isNumber(sort)) {
                return data=DataVo.failure("话题参数错误");
            }
            data = userGroupService.updateGroupSort(Integer.valueOf(id),Integer.valueOf(sort));
        } catch (Exception e) {
            data = DataVo.failure(e.getMessage());
        }
        return data;
    }

    @GetMapping(value = "/list")
    public String roleList(@RequestParam(value = "p", defaultValue = "1") int pageNum, ModelMap modelMap){
        PageVo<UserPermission> pageVo=userPermissionService.getPermissionListPage(pageNum,20);
        modelMap.put("pageVo", pageVo);
        modelMap.addAttribute("admin", getAdminUser());
        return theme.getAdminTemplate("user/permission_list");
    }



}
